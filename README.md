## Gusto Export extractor
Extractor component for Keboola Connection allowing to retrieve daily checks from Gusto Export API

### Configuration
- **Gusto user name** – (REQ) Your Gusto account user name
- **Gusto password** - (REQ) Password
- **Gusto customer id** - (REQ) Customer ID
- **Master location id** - (REQ) Master Location ID
- **Gusto vendor id** - (REQ) Gusto vendor id
- **Period from date [including]** – Start date from which to retrieve data (Inclusive)  
- **Period to date [excluding]** – End of the retrieved period (exclusive) 
- **Relative period from now (utc)** – Relative period in format: '5 hours ago', 'yesterday','3 days ago', '4 months ago', '2 years ago', 'today'. Overrides `from` and `to` parameters. NOTE: Either `Relative period` parameter or pair of `Period From` and `To` parameters are required.
- **Continue since last start** - If set to `Yes` every consecutive run starts where the last one ended.
- **Backfill mode** – This switch is to help backloading historical data. Depending on your filter setup, the extract for couple of days period may take quite a lot time. Component `run-time limit is 4 hours`. To backload data for few years back this helps to setup a regular orcehstration and load all historical data in appropriate chunks (<4hrs run). If backfill mode is enabled, each consecutive run of the component will continue from the end of the last run period, until current date is reached. The `From` and `To` date parameters are used to define Start date of the back-fill and also relative window of each run. Once the current date is reached, everything since last run date is downloaded on the next run.

