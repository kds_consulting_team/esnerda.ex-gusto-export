from gusto.client import Client
from kbc.mapping import GenericMapping
import requests
import logging
import kbc



class GustoService:
    
    def __init__(self, customer_id, master_location_id, vendor_id, user, password):
        self.client = Client(customer_id, master_location_id, vendor_id, user, password)
        
        
    
    def get_n_save_location(self, output_folder, file_uid, time_stamp, time_delta = None):
        '''
        Saves location data
        
        Return:
        result_files -- Array of dictionary 
            e.g. {'file_path':'data/out/file_1.csv',
                   'name' : file_1.csv,
                    'prefix' : file,
                    'ids' : ['id','parent_id']
                 }
        return_res_cols -- dict of return columns if specified, e.g. {'id':[1,2,3,4]}   
        data_delta = delta parameter for next call
        
        '''
        res = self.client.get_location(data_delta = time_delta)
        ## based on mapping.json
        user_values = {'extractionDate' : time_stamp,
                       'customer_id' : res['req_customer_id']}

        mapping = GenericMapping('location', output_folder, user_values = user_values, return_col = {'location':'id'}, )
        
        res_files, cols = mapping.parse_n_save(res['data']['location'], file_uid)
        return res_files, cols
    
    
    def get_n_save_order_type(self, output_folder, file_uid, time_stamp, loc_id = None):
        '''
        Saves location data
        
        Return:
        result_files -- Array of dictionary 
            e.g. {'file_path':'data/out/file_1.csv',
                   'name' : file_1.csv,
                    'prefix' : file,
                    'ids' : ['id','parent_id']
                 }
        return_res_cols -- dict of return columns if specified, e.g. {'id':[1,2,3,4]}   
        data_delta = delta parameter for next call
        
        '''
        res = self.client.get_order_type(master_location_id=loc_id)
        ## based on mapping.json
        user_values = {'extractionDate' : time_stamp,
                       'customer_id' : res['req_customer_id']}

        mapping = GenericMapping('order_type', output_folder, user_values = user_values, return_col = {'location':'id'}, )
        
        res_files, cols = mapping.parse_n_save(res['data']['order_type'], file_uid)
        return res_files
    
    def get_n_save_category(self, output_folder, file_uid, time_stamp, loc_id = None):
        '''
        Saves location data
        
        Return:
        result_files -- Array of dictionary 
            e.g. {'file_path':'data/out/file_1.csv',
                   'name' : file_1.csv,
                    'prefix' : file,
                    'ids' : ['id','parent_id']
                 }
        return_res_cols -- dict of return columns if specified, e.g. {'id':[1,2,3,4]}   
        data_delta = delta parameter for next call
        
        '''
        res = self.client.get_category(master_location_id=loc_id)
        ## based on mapping.json
        user_values = {'extractionDate' : time_stamp,
                       'customer_id' : res['req_customer_id']}

        mapping = GenericMapping('category', output_folder, user_values = user_values, return_col = {'location':'id'}, )
        
        res_files, cols = mapping.parse_n_save(res['data']['category'], file_uid)
        return res_files
    
    def get_n_save_checks(self, output_folder, file_uid, loc_id, start_date, end_date, time_delta = None, store_as_sliced = False):
        '''
        Saves checks into output folder
        Return:
        result_files -- Array of dictionary 
            e.g. {'file_path':'data/out/file_1.csv',
                   'name' : file_1.csv,
                    'prefix' : file,
                    'ids' : ['id','parent_id']
                 }
        return_res_cols -- dict of return columns if specified, e.g. {'id':[1,2,3,4]}   
        data_delta = delta parameter for next call
        """
        '''
        try:
            res = self.client.get_checks(location_id = loc_id, start_date=start_date, end_date=end_date, data_delta=time_delta)
        except requests.HTTPError as e:
            raise e

        data_delta = res['data_delta']
        ## based on mapping.json
        user_values = {'user_index' : ['parent_table', kbc.mapping.KEY_ROW_NUMBER],
                       'customer_id' : res['req_customer_id'],
                       'location_id' : res['req_location_id']}
        mapping = GenericMapping('checks', output_folder, user_values = user_values)
        if not res['data'].get('check'):
            logging.warning('Location %s contains no checks for period %s-%s', loc_id, start_date, end_date)
            logging.debug(res)
            return [], {}, None
        res_files, cols = mapping.parse_n_save(res['data']['check'], file_uid, store_as_sliced = store_as_sliced)
        
        return res_files, cols, data_delta
        
       
        
        
        
        
        
        
        
        