'''
Created on 15. 10. 2018

@author: esner
'''
from kbc.client_base import HttpClientBase

DEAFULT_BASE_V1 = 'https://gustoexportprod.azurewebsites.net/'
BASE_ENDPOINT_MASK = 'export/v1/{}/{}'

BASE_ENDPOINT_URL_MASK = DEAFULT_BASE_V1 + BASE_ENDPOINT_MASK

KEY_VENDOR_ID = 'vendor_id'
KEY_DATATYPE = 'data_type'
KEY_SUBTYPE = 'sub_data_type'
KEY_DATA_DELTA = 'delta_from'

KEY_LOCATION = 'location'
KEY_CHECKS = 'checks'
KEY_ORDER_TYPE = 'order_type'
KEY_CATEGORY = 'category'

CONFIG_DATA_TYPE = 'config'

MAX_RETIRES = 10
STATUS_FORCE_LIST = (500, 502, 504)


class Client(HttpClientBase):

    def __init__(self, customer_id, master_location_id, vendor_id, user, password, base_url=DEAFULT_BASE_V1):

        HttpClientBase.__init__(
            self, base_url, max_retries=MAX_RETIRES, status_forcelist=STATUS_FORCE_LIST, auth = (user, password))
        self._customer_id = customer_id
        self._master_location_id = master_location_id
        self._vendor_id = vendor_id

    def get_location(self, customer_id = None, master_location_id = None, 
                   vendor_id = None, data_delta = None):

        return self._get_request(customer_id, master_location_id,
                          vendor_id, CONFIG_DATA_TYPE, KEY_LOCATION, data_delta)
    
    def get_category(self, customer_id = None, master_location_id = None, 
                   vendor_id = None, data_delta = None):

        return self._get_request(customer_id, master_location_id,
                          vendor_id, CONFIG_DATA_TYPE, KEY_CATEGORY, data_delta)

    def get_order_type(self, customer_id = None, master_location_id = None, 
                   vendor_id = None, data_delta = None):

        return self._get_request(customer_id, master_location_id,
                          vendor_id, CONFIG_DATA_TYPE, KEY_ORDER_TYPE, data_delta)
        
    def get_checks(self, location_id = None, customer_id = None, 
                   vendor_id = None, data_delta = None, start_date = None, end_date = None):
        
        add_params = {'start_date' : start_date,
                      'end_date' : end_date}
        
        
        return self._get_request(customer_id, location_id,
                          vendor_id, KEY_CHECKS, None, data_delta, **add_params)
        
    

    def _get_request(self, customer_id, master_location_id, vendor_id, req_data_type, req_sub_data_type, data_delta, **additional_params):

        # default vaules if empty
        if not customer_id:
            customer_id = self._customer_id
        if not master_location_id:
            master_location_id = self._master_location_id
        if not vendor_id:
            vendor_id = self._vendor_id
            
        url = BASE_ENDPOINT_URL_MASK.format(customer_id, master_location_id)

        params = {KEY_VENDOR_ID: vendor_id,
                  KEY_DATATYPE: req_data_type,
                  KEY_SUBTYPE: req_sub_data_type,
                  KEY_DATA_DELTA: data_delta}
        params.update(additional_params)

        return self.get(url, params)
