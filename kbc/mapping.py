"__author__ = 'Leo Chan'"
from _collections import OrderedDict
"__credits__ = 'Keboola 2017'"

"""
Python 3 environment 
"""

import os
import logging
import json
import pandas as pd
import uuid



# used for parent-child key mapping
KEY_PARENT_TABLE = 'parent_table' 
KEY_UUID = 'uuid'
KEY_ROW_NUMBER = 'row_number'

### destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"#
DEFAULT_MAPPING_PATH = '/code/generic-mapping/mappings.json'
#DEFAULT_MAPPING_PATH = 'generic-mapping/mappings.json'

class GenericMapping():
    """
    Handling Generic Ex Mapping
    """

    def __init__(self, endpoint, output_destination, user_values = {}, return_col = {}, mapping_file_path = DEFAULT_MAPPING_PATH):

        ### Parameters
        self._mapping_file_path = mapping_file_path
        self.output_destination = output_destination
        self.endpoint = endpoint
        self.mapping = self.mapping_check(self.endpoint)
        self._user_values = user_values
        self._return_col = return_col
        #self.data = data
        self.out_file = {}
        self.out_file[self.endpoint]=[]
        self.out_file_pk = {} ### destination name from mapping
        self.out_file_pk[self.endpoint]=[]
        self.out_file_pk_raw = {} ### raw destination name from API output
        self.get_primary_key(endpoint, self.mapping)
        logging.debug(self.out_file_pk)
        logging.debug(self.out_file_pk_raw)


    def parse_n_save(self, data, file_uid, store_as_sliced = False):
        ### Runs
        self.root_parse(data)
        return self.output(file_uid, store_as_sliced)

    
    def mapping_check(self, endpoint):
        """
        Selecting the Right Mapping for the specified endpoint
        """

        with open(self._mapping_file_path, 'r') as f:
            out = json.load(f)
        f.close()
        return out[endpoint]

    def root_parse(self, data):
        """
        Parsing the Root property of the return data
        """

        #data = self.data
        mapping = self.mapping

        for index, row in enumerate(data):
            ### Looping row by row 
            self.parsing(self.endpoint, mapping, row, index)      

    def parsing(self, table_name, mapping, data, row_number):
        """
        Outputing data results based on configured mapping
        """
        
        ### If new table property is found, 
        ### create a new array to store values
        if table_name not in self.out_file:
            self.out_file[table_name] = []
        

        row_out = {} ### Storing row output

        ### Looping thru the keys of the mapping
        for column in mapping:
            if not mapping[column].get("type") or mapping[column].get("type")=="column":
                ### Delimit mapping variables
                if "." in column:
                    temp_value = column.split(".")

                    try:
                        ### Looping thru the array
                        #value = data[temp_value[0]][temp_value[1]]
                        value = data
                        for word in temp_value:
                            value = value[word]
                    except Exception:
                        value = ""
                else:
                    try:
                        value = data[column]
                    except Exception:
                        value = ""
                header = mapping[column]["mapping"]["destination"]
            
            elif mapping[column]["type"]=="table": 
                ### Setting up table parameters, 
                ### mappings and values for parsing the nested table
                mapping_name = column
                mapping_in = mapping[column]["tableMapping"] ### Mapping for the table
                sub_table_name = mapping[column]["destination"] ### New table output name
                sub_table_exist = True ### Determine if the table column exist as a property in source file
                sub_table_row_exist = True ### Determine if there are any rows within the sub table
                
                ### Passing the function if the JSON property is not found
                try:
                    if "." in mapping_name:
                        temp_value = mapping_name.split(".")
                        data_in = data
                        for word in temp_value:
                            data_in = data_in[word]
                    else:
                        data_in = data[mapping_name]

                    if not data_in or len(data_in)==0:
                        sub_table_row_exist = False
                except KeyError:
                    sub_table_exist = False

                ### Verify if the sub-table exist in the root table
                if sub_table_exist and sub_table_row_exist:
                    ### Setting up nested table primary key
                    ### Using current table id to create unique pk with md5
                    string_of_pk = "" ### Concat all the PK as a string
                    ### Iterate through all the pk
                    """for pk in self.out_file_pk_raw[table_name]:
                        if "." in pk:
                            temp_value = column.split(".")
                            pk_value = data
                            for each_temp in temp_value:
                                pk_value = pk_value[each_temp]
                            print("PK_VALUE - {0}".format(pk_value))
                            print("STRING_OF_PK - {0}".format(string_of_pk))
                            string_of_pk += pk_value
                        else:
                            string_of_pk += data[pk]"""
                    #genrate random uuid if no primary key for parent table is defined, otherwise use pk
                    if self.out_file_pk.get(table_name):
                        
                        pk_values = []
                        for field in self.out_file_pk.get(table_name):
                            # user defined PK
                            if mapping[field].get('type') == 'user_index':
                                value = str(self._validate_user_index_mapping(mapping, data, field, table_name, row_number))
                            # user defined static value
                            elif mapping[field].get('type') == 'user': 
                                value = str(self._user_values.get(column))
                            else:
                                value = str(data[field])
                            pk_values.append(value)
                        
                        sub_table_pk = mapping[column]["destination"] + "-"+ '|'.join(pk_values)
                    else:
                        sub_table_pk = mapping[column]["destination"] + "-"+str(uuid.uuid4().hex)

                    mapping_in[KEY_PARENT_TABLE] = {
                        "type": "pk",
                        "value": sub_table_pk
                    }

                    ### Loop nested table
                    self._parse_table(sub_table_name, mapping_in, data_in)
                    
                    ### Returning sub table PK
                    value = sub_table_pk
                
                else:
                    value = ""

                ### Primary key return to the root table
                header = column
                
            ### Sub table's Primary Key
            ### Source: injected new property when new table is found in the mapping
            elif mapping[column]["type"]=="pk":
                # look if destination is overriden in mapping
                if mapping.get('parentKey',{}).get('destination'):
                    header = mapping.get('parentKey').get('destination')
                else:
                    header = column
                value = mapping[column]["value"]
            
            elif mapping[column]["type"]=="user":
                header = column
                value = self._user_values.get(column)
            elif mapping[column]["type"]=="user_index":
                header = column
                ## validate
                value = self._validate_user_index_mapping(mapping, data, column, table_name, row_number)
                
            else:
                print('Unsupported type ' + mapping[column]["type"])

            ### Injecting new table elements for the row
            row_out[header] = value

        ### Storing JSON tables
        out_file = self.out_file
        out_file[table_name].append(row_out)
        self.out_file = out_file

        return

    def _validate_user_index_mapping(self, mapping, data, column, table_name, index):

        index_keys = self._user_values.get("user_index")
        if not index_keys:
            raise ValueError(
                    'No index fields specified for "user_index mapping" name:' + column)
        missing_keys = [index for index in index_keys if not data.get(index) and index not in [KEY_PARENT_TABLE, KEY_UUID, KEY_ROW_NUMBER]]
        if missing_keys:
            raise ValueError('Some keys specified in "user_index" mapping are missing in data: [{}]'.format(
                    ','.join(missing_keys)))
        if KEY_PARENT_TABLE in index_keys and not mapping.get(KEY_PARENT_TABLE):
            raise ValueError(
                    '"parent_key" is missing in (sub)table mapping but is specified, table [{}]'.format(table_name))

        value = '|'.join([str(data[col])
                          for col in index_keys if col not in [KEY_PARENT_TABLE, KEY_UUID, KEY_ROW_NUMBER]])
        if KEY_PARENT_TABLE in index_keys:
            value += '|' + mapping[KEY_PARENT_TABLE]['value']
        # uuid if present
        if KEY_UUID in index_keys:
            value += "-"+str(uuid.uuid4().hex)
        # row_number if present
        if KEY_ROW_NUMBER in index_keys:
            value += "-" + str(index)
            
        # return hash
        return value


    def _parse_table(self, table_name, mapping, data):
        """
        Parsing table data
        Determining the type of the sub-table
        *** Subfunction of parse() ***
        """

        if type(data) == dict:
            self.parsing(table_name, mapping, data, None)

        elif type(data) == list:
            for index, row in enumerate(data):
                self.parsing(table_name, mapping, row, index)

        return

    def get_primary_key(self, table_name, mapping):
        """
        Filtering out all the primary keys within the mapping table
        """

        ### If table_name does not exist in the PK list
        if table_name not in self.out_file_pk_raw:
            self.out_file_pk_raw[table_name] = []
            self.out_file_pk[table_name] = []

        for column in mapping:
            ### Column type is "column"
            if not mapping[column].get("type") or mapping[column].get("type") in ["column", 'user_index']:
                ### Search if the priamryKey property is within the mapping configuration
                if "primaryKey" in mapping[column]["mapping"].keys():
                    ### Confirm if the primary key tab is true
                    if mapping[column]["mapping"]["primaryKey"]:
                        self.out_file_pk_raw[table_name].append(column)
                        self.out_file_pk[table_name].append(mapping[column]["mapping"]["destination"])
            ### Column type is "table"
            elif mapping[column]["type"]=="table":
                ## add parentKey if primary
                if mapping[column].get('parentKey',{}).get('primaryKey', False):
                    self.out_file_pk_raw[mapping[column]["destination"]]=[column]
                    self.out_file_pk[mapping[column]["destination"]] = [mapping[column]["parentKey"]['destination']]
                ### Recursively run the tableMapping
                self.get_primary_key(table_name=mapping[column]["destination"], mapping=mapping[column]["tableMapping"])

        return

    def produce_manifest(self, file_name, primary_key):
        """
        Dummy function to return header per file type.
        """

        file = "/data/out/tables/"+str(file_name)+".manifest"
        logging.debug("Manifest output: {0}".format(file))
        #destination_part = file_name.split(".csv")[0]

        manifest_template = {
                            #"source": "myfile.csv"
                            #,"destination": "in.c-mybucket.table"
                            #"incremental": bool(incremental)
                            #,"primary_key": ["VisitID","Value","MenuItem","Section"]
                            #,"columns": [""]
                            #,"delimiter": "|"
                            #,"enclosure": ""
                            }

        column_header = []

        manifest = manifest_template
        #manifest["primary_key"] = primary_key

        try:
            with open(file, 'w') as file_out:
                json.dump(manifest, file_out)
                #logging.info("Output manifest file ({0}) produced.".format(file_name))
        except Exception as e:
            logging.error("Could not produce output file manifest.")
            logging.error(e)
        
        return

    def output(self, file_uid, store_as_sliced):
        """
        Output Data with its desired file name
        Return:
        result_files -- Array of dictionary 
            e.g. {'file_path':'data/out/file_1.csv',
                   'name' : file_1.csv,
                    'prefix' : file,
                    'ids' : ['id','parent_id']
                 }
        return_res_cols -- dict of return columns if specified, e.g. {'id':[1,2,3,4]}   
        """
        
        ### Outputting files
        result_files = []
        return_res_cols = OrderedDict()
        out_file = self.out_file
        for file in out_file:
            out_df = pd.DataFrame(out_file[file], dtype=str)
            if file == 'check-payment_refund':
                logging.debug('processing modifier')
            # skip if result empty
            if (out_df.empty):
                logging.info('%s is empty, skipping.',out_file[file])
                return [], []
            # get return values if any
            if isinstance(self._return_col, dict) and self._return_col.get(file):
                return_res_cols.update({file: 
                                        {self._return_col.get(file) : out_df[self._return_col.get(file)].tolist()}
                                        })
            suffix = ''
            if file_uid:
                suffix = '_' + str(file_uid)
            file_name = file + suffix + ".csv"
            if store_as_sliced:
                dest_dir = os.path.join(self.output_destination, file)
                if not os.path.exists(dest_dir):
                    os.makedirs(dest_dir)
                file_dest = os.path.join(dest_dir, file_name)
            else:
                file_dest = os.path.join(self.output_destination, file_name)

            out_df.to_csv(file_dest, index=False)
            logging.debug("Table output: {0}...".format(file_dest))
            result_files.append({'file_path' : file_dest,
                                 'name' : file_name,
                                 'prefix' : file,
                                 'ids' : self.out_file_pk[file]
                                 })



        return result_files, return_res_cols
