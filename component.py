'''

@author: esner
'''
from kbc.env_handler import KBCEnvHandler
from gusto.client_service import GustoService
import logging
import math
import pytz

from datetime import datetime
from datetime import timedelta

KEY_USER = 'user'
KEY_PASS = '#pass'
KEY_CUSTOMER_ID = 'customer_id'
KEY_MASTER_LOCATION_ID = 'master_location_id'
KEY_VENDOR_ID = '#vendor_id'


KEY_PERIOD_FROM = 'period_from'
KEY_PERIOD_TO = 'period_to'
KEY_RELATIVE_PERIOD = 'relative_period'
KEY_BACKFILL_MODE = 'backfill_mode'
KEY_MAND_PERIOD_GROUP = [KEY_PERIOD_FROM, KEY_PERIOD_TO]
KEY_MAND_DATE_GROUP = [KEY_RELATIVE_PERIOD, KEY_MAND_PERIOD_GROUP]

MAX_PERIOD_SIZE = 10

MANDATORY_PARS = [KEY_USER, KEY_PASS, KEY_CUSTOMER_ID,
                  KEY_MASTER_LOCATION_ID, KEY_VENDOR_ID, KEY_MAND_DATE_GROUP]

APP_VERSION = '0.1.6'


class Component(KBCEnvHandler):

    def __init__(self, data_path = None):
        KBCEnvHandler.__init__(self, MANDATORY_PARS, data_path = data_path)

    def run(self, debug=False):
        '''
        Main execution code
        '''
        # setup
        self.set_default_logger('DEBUG' if debug else 'INFO')
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')
        self.validateConfig()
        params = self.cfg_params
        backfill_mode = params.get(KEY_BACKFILL_MODE)
        if backfill_mode and not (params.get(KEY_PERIOD_FROM) and params.get(KEY_PERIOD_TO)):
            logging.error(
                'Period from and Period To paremeters must be defined when backfill mode is on!')
            exit(1)

        gservice = GustoService(params[KEY_CUSTOMER_ID], params[KEY_MASTER_LOCATION_ID],
                                params[KEY_VENDOR_ID], params[KEY_USER], params[KEY_PASS])

        extract_time = datetime.utcnow()
        start_date, end_date, periods = self._get_date_periods()

        last_delta = None
        last_state = self.get_state_file()
        if last_state and params['since_last'] and last_state['data_delta'] and last_state['data_delta'] != 'None':
            start_date = datetime.strptime(last_state['data_delta'], '%m%d%Y')
            end_date = datetime.utcnow()            
            last_delta = last_state['data_delta']
        elif self.cfg_params.get(KEY_RELATIVE_PERIOD):
            from_date = super().get_past_date(self.cfg_params.get(KEY_RELATIVE_PERIOD))
            last_delta = from_date.strftime("%m%d%Y")

        # get locations
        logging.info('Getting locations..')
        res_files, loc_ids = gservice.get_n_save_location(
            self.tables_out_path, None, extract_time)
        # create manifests
        self._create_manifests(res_files)

        
        # get locations
        logging.info('Getting categories..')
        res_files = gservice.get_n_save_category(
            self.tables_out_path, None, extract_time)
        # create manifests
        self._create_manifests(res_files)

        
        # get locations
        logging.info('Getting order types..')
        res_files = gservice.get_n_save_order_type(
            self.tables_out_path, None, extract_time)
        # create manifests
        self._create_manifests(res_files)

        logging.info('Fetching checks data for period %s - %s', start_date.date(), end_date.date())
        index = 0
        res_files = []
        for loc_id in loc_ids['location']['id']:
            # skip master locations
            if str(loc_id) == params[KEY_MASTER_LOCATION_ID]:
                continue

            logging.info('Downloading checks for location id:%s', loc_id)

            index += 1
            if last_delta and (params['since_last'] or params[KEY_RELATIVE_PERIOD]):
                res_files_local, cl, delta = gservice.get_n_save_checks(self.tables_out_path, loc_id, loc_id,
                                                                        None, None, time_delta=last_delta, store_as_sliced=True)
            else:
                res_files_local, delta = self._get_checks_in_periods(
                    loc_id, periods, gservice)

            res_files += res_files_local

        # create manifests
        logging.info('Preparing checks sliced tables')
        self._create_manifests_sliced(res_files)
        # write statefile
        self.write_state_file({'data_delta': str(delta),
                               'last_period': {'start_date': str(start_date.date()),
                                                   'end_date': str(end_date.date())}})

        
        logging.info('Extraction finshed sucessfully')

    def _get_checks_in_periods(self, loc_id, periods, gservice):
        res_in_period = []
        last_delta = ''
        res_files = []
        for period in periods:
            uid = str(loc_id) + '_' + \
                period['start_date'] + '_' + period['end_date']
            res_files, cols, delta = gservice.get_n_save_checks(self.tables_out_path, uid, loc_id,
                                                                period['start_date'], period['end_date'], store_as_sliced=True)
            last_delta = delta
            res_in_period += res_files
        return res_in_period, last_delta

    def _get_date_periods(self):
        periods = []
        if self.cfg_params.get(KEY_BACKFILL_MODE):
            start_date, end_date = self._get_backfill_period()
            periods = list(self._split_dates_to_chunks(
                start_date, end_date, MAX_PERIOD_SIZE))

        elif self.cfg_params.get(KEY_RELATIVE_PERIOD):
            start_date = super().get_past_date(self.cfg_params.get(KEY_RELATIVE_PERIOD))
            end_date = datetime.now()
            periods = list(self._split_dates_to_chunks(
                start_date.date(), end_date.date(), MAX_PERIOD_SIZE))

        elif self.cfg_params.get(KEY_PERIOD_FROM) and self.cfg_params.get(KEY_PERIOD_TO):
            start_date = datetime.strptime(
                self.cfg_params.get(KEY_PERIOD_FROM), '%Y-%m-%d')
            end_date = datetime.strptime(
                self.cfg_params.get(KEY_PERIOD_TO), '%Y-%m-%d')
            periods = list(self._split_dates_to_chunks(
                start_date, end_date, MAX_PERIOD_SIZE))
            

        elif self.cfg_params.get(KEY_PERIOD_FROM) and self.cfg_params.get(KEY_PERIOD_TO):
            start_date = datetime.strptime(
                self.cfg_params.get(KEY_PERIOD_FROM), '%Y-%m-%d')
            end_date = datetime.strptime(
                self.cfg_params.get(KEY_PERIOD_TO), '%Y-%m-%d')
            periods = list(self._split_dates_to_chunks(
                start_date, end_date, MAX_PERIOD_SIZE))

        else:
            end_date = datetime.utcnow()
            start_date = self.get_past_date(
                '14 days ago', end_date)
            periods = [{'start_date': start_date.strftime("%m%d%Y"),
                        'end_date': end_date.strftime("%m%d%Y")}]
        return start_date, end_date, periods

    def _split_dates_to_chunks(self, start_date, end_date, intv, strformat="%m%d%Y"):
        nr_days = (end_date - start_date).days
        if nr_days < intv:
            yield {'start_date': start_date.strftime(strformat),
                   'end_date': end_date.strftime(strformat)}
        else:            
            diff = (end_date - start_date) / (math.ceil(nr_days/intv))
            for i in range(math.ceil(nr_days/intv)):
                yield {'start_date': (start_date + diff * i).strftime(strformat),
                       'end_date': (start_date + diff * (i + 1)).strftime(strformat)}

    def _create_manifests(self, res_files):
        for file in res_files:
            self.configuration.write_table_manifest(
                file_name=file['file_path'],
                destination=file['prefix'],
                primary_key=file['ids'])

    def _create_manifests_sliced(self, res_files):

        res_sliced_folders = {}
        for file in res_files:
            res_sliced_folders.update({file['prefix']: (file['ids'])})

        for folder in res_sliced_folders:
            self.create_sliced_tables(folder, res_sliced_folders[folder], True)
    
    def _get_backfill_period(self):
        state = self.get_state_file()
        
        if state and state.get('last_period'):
            last_start_date = datetime.strptime(
                state['last_period']['start_date'], '%Y-%m-%d')
            last_end_date = datetime.strptime(
                state['last_period']['end_date'], '%Y-%m-%d')
            
            diff = last_end_date - last_start_date
            # if period is a single day
            if diff.days == 0:
                diff = timedelta(days=1)

            start_date = last_end_date
            if (last_end_date.date() + diff) >= datetime.now(pytz.utc).date() + timedelta(days=1):
                end_date = datetime.now(pytz.utc) + timedelta(days=1)
            else:
                end_date = last_end_date + diff
        else:
            start_date = datetime.strptime(
                self.cfg_params.get(KEY_PERIOD_FROM), '%Y-%m-%d')
            end_date = datetime.strptime(
                self.cfg_params.get(KEY_PERIOD_TO), '%Y-%m-%d')
        return start_date, end_date


"""
        Main entrypoint
"""
if __name__ == "__main__":
    comp = Component()
    comp.run()
